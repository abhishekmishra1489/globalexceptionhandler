package com.abhicode.repository;

import com.abhicode.model.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CityRepository extends CrudRepository<City, Long> {
    /*List<City> findAll() ;*/
}

/*public interface CityRepository extends JpaRepository<City , Long> {
}*/
