package com.abhicode.service;

import com.abhicode.exception.CityNotFoundException;
import com.abhicode.exception.NoDataFoundException;
import com.abhicode.model.City;
import com.abhicode.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CityService implements ICityService {


    private CityRepository cityRepository;

    @Autowired
    public CityService(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public City findById(Long id) {

        return cityRepository.findById(id)
                .orElseThrow(() -> new CityNotFoundException(id));

    }

    @Override
    public City save(City city) {

        return cityRepository.save(city);

    }

    @Override
    public List<City> findAll() {

        Iterable<City> iterable = cityRepository.findAll();
        List<City> list = StreamSupport
                .stream(iterable.spliterator(), false)
                .collect(Collectors.toList());


        return Optional.of(list)
                .filter(cities -> !cities.isEmpty())
                .orElseThrow(NoDataFoundException::new);

    }
}
