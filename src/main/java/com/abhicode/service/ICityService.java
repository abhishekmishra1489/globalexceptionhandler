package com.abhicode.service;

import com.abhicode.model.City;

import java.util.List;

public interface ICityService {
     City findById(Long id) ;
     City save(City city) ;
     List<City> findAll() ;
}
